<?php

abstract class Auth
{
    var $session_name = '';
    var $users = array();
    var $session_user = '';
    var $session_hash = '';

    function __construct($session_name, $users, $session_user, $session_hash)
    {
        $this->session_name = $session_name;
        $this->users = $users;
        $this->session_user = $session_user;
        $this->session_hash = $session_hash;
    }

    function Check()
    {
        if (isset($_SESSION[$this->session_user]) && isset($_SESSION[$this->session_hash])
            && sha1($this->users[$_SESSION[$this->session_user]] . sha1(date('YmdH')) . sha1($this->IPDetect())) == $_SESSION[$this->session_hash]) {
            return true;
        } else {
            return false;
        }
    }

    function TryLogin()
    {
        if (isset($_POST[$this->session_user]) && isset($_POST[$this->session_hash])) {
            $_SESSION[$this->session_user] = $_POST[$this->session_user];
            $_SESSION[$this->session_hash] = sha1(sha1($_POST[$this->session_hash]) . sha1(date('YmdH')) . sha1($this->IPDetect()));
        }
    }

    function TryExit()
    {
        if (isset($_POST['exit']) || isset($_GET['exit'])) {
            $_SESSION[$this->session_user] = '';
            $_SESSION[$this->session_hash] = '';
            session_destroy();
            return true;
        } else {
            return false;
        }
    }

    function Login()
    {
        session_name($this->session_name);
        session_start();
        if ($this->TryExit()) {
            $this->ExitForm();
        } else {
            $this->TryLogin();
            if ($this->Check()) {
                $this->Content();
            } else {
                $this->LoginForm();
            }
        }
    }

    private function IPDetect()
    {
        $serverVars = array(
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "HTTP_X_COMING_FROM",
            "HTTP_COMING_FROM",
            "HTTP_CLIENT_IP",
            "HTTP_XROXY_CONNECTION",
            "HTTP_PROXY_CONNECTION",
            "HTTP_USERAGENT_VIA"
        );
        foreach ($serverVars as $serverVar) {
            if (!empty($_SERVER) && !empty($_SERVER[$serverVar]))
                $proxyIP = $_SERVER[$serverVar];
            elseif (!empty($_ENV) && isset($_ENV[$serverVar]))
                $proxyIP = $_ENV[$serverVar];
            elseif (@getenv($serverVar))
                $proxyIP = getenv($serverVar);
        }
        if (!empty($proxyIP)) {
            $isIP = preg_match('|^([0-9]{1,3}\.){3,3}[0-9]{1,3}|', $proxyIP, $regs);
            $long = ip2long($regs[0]);
            if ($isIP && (sizeof($regs) > 0) && $long != -1 && $long !== false)
                return $regs[0];
        }
        return $_SERVER['REMOTE_ADDR'];
    }
}