<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 07.09.18
 * Time: 19:27
 */
namespace src;

use model\Bonuses;
use model\Prize;

require_once "DB.php";
class Lucky
{
    public $types = [
        'money', 'bonus', 'thing'
    ];
    public $moneyPrize = '';


    public function __construct()
    {
        $this->getAuth();
        $this->moneyPrize = new MoneyPrize();
    }

    public function getAuth()
    {
        require_once 'User.php';

        if (\User::isGuest()) {
            require_once '../views/guest.php';
        } else {
            $userId = \User::checkLogged();
            $prize = $this->playLottery($userId);

            //TODO send $prize to view.. To template
        }

        return true;
    }

    public function playLottery($userId)
    {
        $typePrize = $this->types[rand(0, count($this->types)-1)];

        $prize = [];
        switch ($typePrize) {
            case 'money':
                $limitMoney = $this->moneyPrize->getLimit();
                $prize[$typePrize] = rand(1, $limitMoney/2);
                $currentMoney = $this->moneyPrize->getCurrentMoneyCount();
                if ($currentMoney) {
                    $this->moneyPrize->updatePrizeOwner($userId, $currentMoney + $prize[$typePrize]);
                } else {
                    $this->moneyPrize->setPrizeOwner($userId, $prize[$typePrize]);
                }
                break;
            case 'bonus':
                //TODO make interval min max flexible
                $min = 1;
                $max = 100;

                $bonus = new Bonuses();
                $amountBonuses = $bonus->getUserBonusesById($userId);
                $newBonuses = rand($min, $max);

                if ($amountBonuses) {
                    $bonus->updateUserBonuses($userId, $amountBonuses+$newBonuses);
                } else {
                    $bonus->setUserBonuses($userId, $newBonuses);
                }
                $prize[$typePrize] = $bonus->getUserBonusesById($userId);
                break;

            case 'thing':
                $prizes = new ThingPrize();
                $prizeId = array_rand($prizes->getNotEmptyPrizes(), 1);

                $prize[$typePrize] = $prizes->getNotEmptyPrizes($prizeId);

                $userPrizes = explode(", ", $prizes->getUserPrizesById($userId));
                $userPrizes[] = $prize[$typePrize];

                $prizes->setPrizeOwner($userId, implode(", ", $userPrizes));
                break;
        }

        return $prize;
    }


}