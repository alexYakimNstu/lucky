<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.09.18
 * Time: 14:54
 */

namespace src;


use model\Money;

class MoneyPrize extends AbstractPrize
{

    public $money;

    public function __construct()
    {
        $this->money = new Money();

    }

    public function getLimit()
    {
        return $this->money->getAvailableMoney();
    }

    public function updatePrizeOwner($userId, $count)
    {
        $this->money->updateMoneyForUserById($userId, $count);
    }

    public function setPrizeOwner($userId, $count)
    {
        $this->money->setMoneyForUserById($userId, $count);
    }

    public function getCurrentMoneyCount($userId)
    {
        return $this->money->getUserMoneyById($userId);
    }
}