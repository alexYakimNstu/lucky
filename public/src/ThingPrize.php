<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.09.18
 * Time: 14:57
 */

namespace src;


use model\Prize;

class ThingPrize extends AbstractPrize
{

    public $thingPrize;
    public function __construct()
    {
        $this->thingPrize = new Prize();
    }

    public function getNotEmptyPrizes()
    {
        return $this->thingPrize->getNotEmptyPrizes();
    }

    public function setPrizeOwner($userId, $prices)
    {
        $this->thingPrize->updateUserPrizesById($userId, $prices);
    }

    public function getUserPrizesById($userId)
    {
        return $this->thingPrize->getPrizeById($userId);
    }

    public function setUserPrizes($userId, $prizes)
    {
        return $this->thingPrize->updateUserPrizesById($userId, $prizes);
    }

}