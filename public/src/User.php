<?php

/**
 * USER - for work with users
 */
class User
{

    /**
     * Remember user
     * @param integer $userId
     */
    public static function auth($userId)
    {
        $_SESSION['user'] = $userId;
    }

    /**
     * Return user id, else go to login page
     * @return string user id
     */
    public static function checkLogged()
    {
        if (isset($_SESSION['user']))
        {
            return $_SESSION['user'];
        }

        header("Location: /cabinet/");
    }

    /**
     * Check user for guest
     * @return boolean
     */
    public static function isGuest()
    {
        if (isset($_SESSION['user']))
        {
            return false;
        }
        return true;
    }

}