<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 09.09.18
 * Time: 12:30
 */

namespace model;


class Bonuses
{
    public $connection;
    public function __construct()
    {

    }

    public function getAuth()
    {
        require_once 'DB.php';

        $db = new \DB();
        $this->connection = $db->connection();

        return $this;
    }

    public function getUserBonusesById($userId)
    {
        if (!is_int($userId)) {
            return;
        }
        $query = "SELECT * FROM bonuses WHERE user_id = $userId";

        try {
            $result = mysql_query($query);
        } catch(\Exception $exception) {
            throw new \Exception('Query failed:'  . mysql_error());
        }

        return $result;
    }

    public function setUserBonuses($userId, $bonuses)
    {
        if (!is_int($userId) || !is_int($bonuses)) {
            return;
        }
        $query = "INSERT INTO
    `bonuses` (`user_id`, `bonuses_count`)
VALUES
    ($userId, $bonuses)";

        try {
            mysql_query($query);
        } catch(\Exception $exception) {
            throw new \Exception('Query failed:'  . mysql_error());
        }

    }

    public function updateUserBonuses($userId, $bonuses)
    {
        if (!is_int($userId) || !is_int($bonuses)) {
            return;
        }
        $query = "UPDATE `bonuses` SET bonuses_count = $bonuses WHERE `id` = $userId";

        try {
            mysql_query($query);
        } catch(\Exception $exception) {
            throw new \Exception('Query failed:'  . mysql_error());
        }

    }

}