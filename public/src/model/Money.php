<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 09.09.18
 * Time: 18:21
 */

namespace model;


class Money
{
    public $connection;

    public function __construct()
    {

    }


    public function getUserMoneyById($userId)
    {
        if (!is_int($userId)) {
            return;
        }
        $query = "SELECT * FROM money WHERE user_id = $userId";

        try {
            $result = mysql_query($query);
        } catch (\Exception $exception) {
            throw new \Exception('Query failed:' . mysql_error());
        }

        return $result;
    }

    public function updateMoneyForUserById($userId, $count)
    {
        if (!is_int($userId) || !is_int($count)) {
            return;
        }
        $query = "UPDATE `money` SET money_count = $count WHERE `user_id` = $userId";

        try {
            mysql_query($query);
        } catch(\Exception $exception) {
            throw new \Exception('Query failed:'  . mysql_error());
        }
    }

    public function setMoneyForUserById($userId, $count)
    {
        if (!is_int($userId) || !is_int($count)) {
            return;
        }
        $query = "INSERT INTO
        `money` (`user_id`, `money_count`)
        VALUES
        ($userId, $count)";

        try {
            mysql_query($query);
        } catch (\Exception $exception) {
            throw new \Exception('Query failed:' . mysql_error());
        }
    }

    public function getAvailableMoney()
    {
        $query = "SELECT amount_money FROM money_bank WHERE id = 1"; //hard code id = 1, just because prototype :(

        try {
            $result = mysql_query($query);
        } catch (\Exception $exception) {
            throw new \Exception('Query failed:' . mysql_error());
        }

        return $result;
    }

}