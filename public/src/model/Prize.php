<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 09.09.18
 * Time: 13:34
 */

namespace model;


class Prize
{

    public function __construct()
    {

    }

    public function getNotEmptyPrizes()
    {
        $query = "SELECT id FROM prizes WHERE prize_count > 0";

        try {
            $result = mysql_query($query);
        } catch(\Exception $exception) {
            throw new \Exception('Query failed:'  . mysql_error());
        }

        return $result;
    }

    public function getPrizeById($id)
    {
        $query = "SELECT * FROM prizes WHERE id = $id";

        try {
            $result = mysql_query($query);
        } catch(\Exception $exception) {
            throw new \Exception('Query failed:'  . mysql_error());
        }

        return $result;
    }

    public function getUserPrizesById($userId)
    {
        $query = "SELECT prizes FROM user_prizes WHERE user_id = $userId";

        try {
            $result = mysql_query($query);
        } catch(\Exception $exception) {
            throw new \Exception('Query failed:'  . mysql_error());
        }

        return $result;
    }


    public function updateUserPrizesById($userId, $prizes)
    {
        $query = "UPDATE `user_prizes` SET prizes = $prizes WHERE `user_id` = $userId";

        try {
            mysql_query($query);
        } catch(\Exception $exception) {
            throw new \Exception('Query failed:'  . mysql_error());
        }
    }

}