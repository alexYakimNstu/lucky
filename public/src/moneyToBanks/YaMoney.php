<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 09.09.18
 * Time: 21:22
 */

namespace moneyToBanks;


class YaMoney
{

    public function __construct()
    {

    }

    public function sendMoney()
    {

        require_once(dirname(__FILE__) . '/lib/YandexMoney.php');
        define ('CLIENT_ID', '<clientIdhere>');
        define ('REDIRECT_URI', '<RedirectToPage>');
        define ('CLIENT_SECRET', '<secretHash>');

        $ym = new \YandexMoney(CLIENT_ID, './ym.log');

        $waletInfo = $ym->accountInfo($token);

        if($_POST['toid'] != '' && $_POST['sum'] != ''){

            if($_POST['sum'] > $waletInfo->getBalance()){
                $pay_message = 'Error: not enough money';
            }
            else{
                $paymentre = $ym->requestPaymentP2P($token,intval($_POST['toid']),floatval($_POST['sum']));

                if($paymentre->getStatus() != 'success'){
                    $pay_message = 'Error: System error';
                }
                else{
                    $paymentpr = $ym->processPaymentByWallet($token, $paymentre->getRequestId());
                    if($paymentpr->getStatus() != 'success'){
                        $pay_message = 'Error: System error';
                    }
                    else{
                        $waletInfo = $ym->accountInfo($token);
                        $pay_message = 'Money was send';
                    }

                }

            }

        }

        if(isset($pay_message)){
            echo '<script>';
            echo 'alert("'.$pay_message.'");';
            echo '</script>';
        }
    }


}