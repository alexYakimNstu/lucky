<?php

session_start();
if(isset($_SESSION['ERRMSG']) &&is_array($_SESSION['ERRMSG']) &&count($_SESSION['ERRMSG']) >0 ) {
    $err = "<table>";
    foreach($_SESSION['ERRMSG'] as $msg) {
        $err .= "<tr><td>" . $msg . "</td></tr>";
    }
    $err .= "</table>";
    unset($_SESSION['ERRMSG']);
}
?>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<form action='reg.php' method='post'>
    <table align="center">
        <tr>
            <td><?php echo $err; ?></td>
        </tr>
        <tr>
            <td>User name</td>
            <td><input type='text' name='username'></td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td><input type='text' name='email'></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type='password' name='password'></td>
        </tr>
        <tr>
            <td>Repeat password</td>
            <td><input type='password' name='rpassword'></td>
        </tr>
        <tr>
            <td><input type='submit' value='Registration'></td>
            <td><a href="login.php">I have been registered</a></td>
        </tr>
    </table>
</form>
</body>
</html>